package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;


@WebFilter(urlPatterns = {"/edit", "/setting"})
public class LoginFilter implements Filter {

	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;

		// セッションが存在しない場合、NULLを返す
		HttpSession session = httpRequest.getSession(false);
		// ログイン情報
		User loginUser = (User) session.getAttribute("loginUser");

		System.out.println("ログインチェック");

		if (loginUser != null){
			// セッションがNULLでなければ、通常通りの遷移
			chain.doFilter(request, response);
		} else {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);

			// セッションがNULLならば、ログイン画面へ飛ばす
			httpResponse.sendRedirect("./login");
		}
	}

	public void destroy() {
	}

}
